'use strict';

const zlib = require('zlib');
const promise = require('bluebird');

class Zip {
    static gzip(buffer) {
        return new promise((resolve, reject) => {
            zlib.gzip(buffer, (err, data) => {
                err ? reject(err) : resolve(data);
            });
        });
    }
}

module.exports = Zip;
