'use strict';
var passport = require('passport');
var user = require('./user-controller');

class ComponentRoutes {
  static registerRoutes(application) {
    application.get('/v1/user', passport.authenticate('jwt', {session: false}), user.get);
    application.delete('/v1/user/:userid', passport.authenticate('jwt', {session: false}), user.delete);
    application.put('/v1/user/:userid', passport.authenticate('jwt', {session: false}), user.put);
  }
}

module.exports = ComponentRoutes;
