var mongoose   = require("mongoose");
var appConfig = require("../config/settings/settings.app.json");

var configured = false;
var mongoConnections = {};
var mongoOptions = {
  server: { poolSize: 10, socketOptions: { keepAlive: 1, connectTimeoutMS: 60000, socketTimeoutMS: 600000 } }
/*
  ,
  replset: {
    rs_name: "rs-XXXXXX",
    poolSize: 10,
    socketOptions: { keepAlive: 1, connectTimeoutMS : 60000, socketTimeoutMS: 600000 }
  }
  */
};

var currentProvider = null;

class ConnectionProvider {
  static Current() {
    if (currentProvider == null) {
      currentProvider = new ConnectionProviderImpl();
      currentProvider.configureConnections();
    }

    return currentProvider;
  }
}


class ConnectionProviderImpl {
  constructor(){
    this.configured = false;
    this.mongoConnections = {};
  }

  configureConnections(){
    let mongoConfig = appConfig.dbs.mongoUrl;
    for(var i = 0; i < mongoConfig.length; i++){
      if(this.mongoConnections.hasOwnProperty(mongoConfig[i].name))
        continue;

      this.mongoConnections[mongoConfig[i].name] = mongoose.createConnection(mongoConfig[i].url, mongoOptions);
    }

    this.configured = true;
  }

  getConnection(connectionName){
    if(!this.configured)
      this.configureConnections();

    if(this.mongoConnections.hasOwnProperty(connectionName))
      return this.mongoConnections[connectionName];
    else
        return null;
  }
}

module.exports = ConnectionProvider;
