var settingsConfig = require('./settings/settings-config');
var passport = require('passport');
var auth = require('../components/auth/auth-controller');
var user = require('../components/v1/user/user-controller');

var AuthRoutes = require('../components/auth/auth-routes');
var UserRoutes = require('../components/v1/user/user-routes');
var ComponentRoutes = require('../components/v1/component/component-routes');

class RouteConfig {
  static registerRoutes(application) {

    ComponentRoutes.registerRoutes(application);
    AuthRoutes.registerRoutes(application);
    UserRoutes.registerRoutes(application);
  }
};

module.exports = RouteConfig;
