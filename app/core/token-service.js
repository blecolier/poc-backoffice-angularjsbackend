'use strict';

var jwt  = require('jwt-simple');
var appConfig = require("../config/settings/settings.app.json");

function setSecretKey(secretKey){
  var buffer = new Buffer(secretKey);
  return buffer.toString('base64');
}

function getExpirationDate(){
  //According to the RFC, they should be in seconds
  return Date.now()/1000 + tokenTimeout * 60;
}

var tokenTimeout = appConfig.auth.tokenTimeout;
var secret = setSecretKey(appConfig.auth.secret);

class TokenService {

  static extractToken(req) {
   var bearer = req.headers["authorization"];
   return jwt.decode(bearer.split(' ')[1], secret);
 }

  static refreshToken (activeTokenId, payload) {
    payload.exp = getExpirationDate();
    payload.tokenId = activeTokenId;
    return jwt.encode(payload, secret);
  }

  static getSecretKey() {
    return secret;
  }
}

module.exports = TokenService;
