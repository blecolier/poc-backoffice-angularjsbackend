'use strict';
var passport = require('passport');
var component = require('./component-controller');

class ComponentRoutes {
  static registerRoutes(application) {
    application.get('/v1/component', passport.authenticate('jwt', {session: false}), component.get);
    application.get('/v1/component/active', passport.authenticate('jwt', {session: false}), component.getActive);
    application.delete('/v1/component/:componentid', passport.authenticate('jwt', {session: false}), component.delete);
    application.put('/v1/component/:componentid', passport.authenticate('jwt', {session: false}), component.put);
    application.post('/v1/component', passport.authenticate('jwt', {session: false}), component.post);
  }
}

module.exports = ComponentRoutes;
