
describe('AuthRepository Tests', function() {

  var authRepository;

  beforeEach(function() {
    authRepository = require('../../../../app/repositories/auth/auth-repository');
  });

  describe('getAuthData()', function() {

    it('should be a function', function(done) {
      expect(authRepository.getAuthData).to.be.a('function');
      done();
    });

  });
});
