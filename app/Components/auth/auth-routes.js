'use strict';
var passport = require('passport');
var auth = require('./auth-controller');

class AuthRoutes {
  static registerRoutes(application) {
    application.get('/hello', auth.hello);
    application.post('/login', auth.login);
    application.post('/refresh', passport.authenticate('jwt', {session: false}), auth.refreshToken);
    application.post('/logout', passport.authenticate('jwt', {session: false}), auth.logout);
  }
}

module.exports = AuthRoutes;
