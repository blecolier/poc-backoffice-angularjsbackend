
describe('AuthService Tests', function() {

  var authService;

  beforeEach(function() {
    authService = require('../../../../app/services/auth/auth-service');
  });

  describe('lookupAuth', function() {

    it('should be a function', function(done) {
      expect(authService.lookupAuth).to.be.a('function');
      done();
    });

  });
});
