var ConnectionProvider = require("../../../core/connection-provider");
var mongoose   = require("mongoose");
var mongoosePaginate = require('mongoose-paginate');
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var connectionName = 'Admin';
var connection = ConnectionProvider.Current().getConnection(connectionName);

var ComponentSchema = new Schema({
  menuName: String,
  componentName: String,
  controllerName: String,
  role: String,
  isActive: Boolean,
  category: String,
  categoryOrder: Number,
  iconClass: String,
  menuOrder: Number
});

ComponentSchema.plugin(mongoosePaginate);

module.exports = connection.model('Component', ComponentSchema);
