'use strict';
const appConfig = require("../config/settings/settings.app.json");
var currentProvider = null;

class MssqlConnectionProvider {
  static Current() {
    if (currentProvider == null) {
      currentProvider = new ConnectionProviderImpl();
      currentProvider.configureConnections();
    }

    return currentProvider;
  }
}

class ConnectionProviderImpl {
  constructor(){
    this.configured = false;
    this.mssqlConnections = {};
  }

  configureConnections(){
    const mssqlConfig = appConfig.dbs.mssql;
    for(var i = 0; i < mssqlConfig.length; i++) {
      if (this.mssqlConnections.hasOwnProperty(mssqlConfig[i].name)) {
          continue;
      }
      this.mssqlConnections[mssqlConfig[i].name] = mssqlConfig[i].config;
    }

    this.configured = true;
  }

  getConnectionConfig(connectionName) {
    if(!this.configured)
      var connectionName =  this.configureConnections();

    if(this.mssqlConnections.hasOwnProperty(connectionName))
      return this.mssqlConnections[connectionName];
    else
        return null;
  }
}

module.exports = MssqlConnectionProvider;
