var https = require('https');
//var http = require('http');
var express = require('express');
var application = express();
var passport = require('passport');
var bodyParser = require('body-parser');
var routeConfig = require('./route-config');
var authService = require('../components/auth/auth-service');
var settingsConfig = require('./settings/settings-config');
var appConfig = require("./settings/settings.app.json");
var fs = require('fs');


function configureWorker(application) {
  configureApplication(application);
  authService.configure(application);
  routeConfig.registerRoutes(application);

  startServer(application);
}

function configureApplication(application) {
  application.use(bodyParser.urlencoded({ extended: true }));
  application.use(bodyParser.json());
  application.use(passport.initialize());

  application.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, X-Requested-With, Content-Type, Authorization');

    res.set('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.set('Pragma', 'no-cache');
    res.set('Expires', '0');
    res.type('application/json');
    next();
  });
}

function startServer(application) {
  var key = fs.readFileSync('app/backoffice-key.pem');
  var cert = fs.readFileSync('app/backoffice-cert.crt')
  var server = https.createServer({key: key, cert: cert, requestCert: true}, application);
  //var server = http.createServer(application);
  server.listen(settingsConfig.settings.workerPort, settingsConfig.settings.hostName, settingsConfig.settings.queueLength, function() {
    console.log('listening at https://%s:%s', settingsConfig.settings.hostName, settingsConfig.settings.workerPort);
  });
}

configureWorker(application);
