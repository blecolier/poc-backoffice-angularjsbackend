'use strict';

var jwt  = require('jwt-simple');
var appConfig = require("../config/settings/settings.app.json");
var TokenService = require('./token-service');
const promise = require('bluebird');


class AuthorizationService {
  static authorize(req, res, moduleName, moduleRole) {
    let jwt_payload = TokenService.extractToken(req);

    return new promise((resolve, reject) => {

      if (moduleRole == 'Admin' && jwt_payload.role != moduleRole) {
        res.status(403).send({success: false, msg: 'Action not authorized for this user'});
        return;
      }

      for (let i = 0; i < jwt_payload.modules.length; i++) {
        if (jwt_payload.modules[i].moduleName.indexOf(moduleName) > -1) {

          resolve();
          return;
        }
      }

      res.status(403).send({success: false, msg: 'Action not authorized for this user'});
      return;
    });
  }
}

module.exports = AuthorizationService;
