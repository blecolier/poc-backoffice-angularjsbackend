var PermissionCypher = require('../../../core/permissionCypher');

var userService = {
  getPublicInfo: function(users){
    var res = [];

    for(var i = 0; i < users.length; i++){
      res.push({_id: users[i]._id, name: users[i].name, displayName: users[i].displayName, role: users[i].role, isActive: users[i].isActive, modules: PermissionCypher.getJsonObjectFromB64(users[i].permissions)})
    }

    return res;
  },

  transformPermissionInfo: function(modules){
    let jsonPermissions = JSON.stringify(modules);

    return {
      Hash : PermissionCypher.getHashFromJsonString(jsonPermissions),
      Permissions: PermissionCypher.getB64FromJsonString(jsonPermissions)
    };
  }
}
module.exports = userService;
