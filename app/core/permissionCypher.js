var crypto = require('crypto');

'use strict';


class PermissionCypher {
  static getHashFromJsonString(json){
    return crypto.createHash('sha256').update(json, 'utf8').digest('hex');
  }

  static getB64FromJsonString(json){
    return Buffer.from(json).toString('base64');
  }

  static verifyHash(hash, b64){
    return hash === PermissionCypher.getHashFromJsonString(Buffer.from(b64, 'base64').toString('utf8'));
  }

  static getJsonObjectFromB64(b64){
    return JSON.parse(Buffer.from(b64, 'base64').toString('utf8'));
  }
}

module.exports = PermissionCypher;
