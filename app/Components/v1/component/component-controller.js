var authorizationService = require('../../../core/authorization-service');
var tokenService = require('../../../core/token-service');
var paginationParser = require('../../../core/paginationParser');
var Component = require('./component-repository');

var moduleName = 'ComponentManagement';
var role = 'Admin';

var componentController = {
  post: (req, res, next) => {
    authorizationService.authorize(req, res, moduleName, role)
      .then(() => {

        let newComponent = new Component({
          menuName : req.body.menuName,
          componentName : req.body.componentName,
          controllerName : req.body.controllerName,
          role : req.body.role,
          isActive : req.body.isActive,
          category: req.body.category,
          categoryOrder: req.body.categoryOrder,
          iconClass: req.body.iconClass,
          menuOrder: req.body.menuOrder
        });

        newComponent.save(function(err){
          if (err)
            res.status(200).json({success: false, msg: 'Error saving the new component'});
            return;
        });

        res.status(200).json({success: true});
    });
  },

  get: (req, res, next) => {
    authorizationService.authorize(req, res, moduleName, role)
      .then(() => {
        Component.paginate({}, paginationParser.parse(req.query), function(err, result){
          if(err) {
            res.status(200).json({success: false, msg: 'Error obtaining the components'});
            return;
          }

          res.status(200).json({success: true, paginationResult: result});
        });
    });
  },

  // Used by all loged in users in order to construct the navigation menu
  // So no need to check for authorization
  getActive: (req, res, next) => {

    Component.find({ isActive: true, 'categoryOrder': { $exists: true }, 'menuOrder': { $exists: true } })
      .sort({ 'categoryOrder': 1, 'menuOrder': 1 })
      .exec()
      .then(components => {
        res.status(200).json({success: true, components: components});
      })
      .catch(err => {
        res.status(200).json({success: false, msg: 'Error obtaining the components'});
      });
  },

  put: (req, res, next) => {
    authorizationService.authorize(req, res, moduleName, role)
      .then(() => {

        Component.update({_id : req.params.componentid},
            {$set: {
                  menuName : req.body.menuName,
                  componentName : req.body.componentName,
                  controllerName : req.body.controllerName,
                  role : req.body.role,
                  isActive : req.body.isActive,
                  category: req.body.category,
                  categoryOrder: req.body.categoryOrder,
                  iconClass: req.body.iconClass,
                  menuOrder: req.body.menuOrder
                }
            },
            function(err){
              if(err) {
                res.status(200).json({success: false, msg: 'Error updating the component'});
                return;
              }
            });

        res.status(200).json({success: true});
    });
  },

  delete: (req, res, next) => {
    authorizationService.authorize(req, res, moduleName, role)
      .then(() => {
        Component.remove({_id : req.params.componentid}, function(err){
          if(err) {
            res.status(200).json({success: false, msg: 'Error removing the component'});
            return;
          }
        });

        res.status(200).json({success: true});
    });
  }
};

module.exports = componentController;
