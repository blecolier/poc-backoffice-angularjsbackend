var passport = require("passport");
var authService = require('./auth-service');
var tokenService = require('../../core/token-service');


var authController = {
  hello: (req, res, next) => {
    res.status(200).json({success: true, message: "Backoffice. Hello!"});
  },

  login: (req, res, next) => {
    var username = '';
    var password = '';

    if(req.body.credentials){
      username = req.body.credentials.username || '';
      password = req.body.credentials.password || '';
    }

    if(username == '' || password == ''){
      res.status(401).json({ "status": 401, "message": "Invalid credentials" });
      return;
    }

    authService.login(username, password)
      .then(token => {
        res.status(200).json({success: true, token: token});
      })
      .catch(() => {
        res.status(403).send({success: false, msg: 'Authenticaton failed, wrong user or password.'});
      });
  },


  refreshToken: (req, res, next) => {
    authService.refreshToken(req)
      .then(token => {
        res.status(200).json({success: true, token: token});
      })
      .catch( () => {
        res.status(403).send({success: false, msg: 'Authenticaton failed, wrong user or password.'});
      });
  },


  logout: (req, res, next) => {
    authService.logout(req)
      .then(() => {
        res.status(200).json({success: true});
      });
  }
};


module.exports = authController;
