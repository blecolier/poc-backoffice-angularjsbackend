'use strict';


class PaginationParser {
    static parse(queryString) {
      if(queryString === undefined || queryString._offset === undefined || queryString._limit === undefined)
        return {offset: 0, limit: 10};

      return {offset: queryString._offset, limit: queryString._limit};
    }
}

module.exports = PaginationParser;
