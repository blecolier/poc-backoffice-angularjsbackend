var ConnectionProvider = require("../../core/connection-provider");
var PermissionCypher = require("../../core/permissionCypher");
var mongoose = require("mongoose");
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var connectionName = 'Admin';
var connection = ConnectionProvider.Current().getConnection(connectionName);


var AuthUserSchema = new Schema({
  name: String,
  displayName: String,
  name: String,
  activeTokenId: String,
  role: String,
  isActive: Boolean,
  permissions: String,
  permissionHash: String
});


AuthUserSchema.plugin(mongoosePaginate);

AuthUserSchema.method('refreshUserActiveToken', function(tokenId){
  this.activeTokenId = tokenId
  this.save();
});

AuthUserSchema.method('rejectActiveToken', function(){
  this.activeTokenId = ''
  this.save();
});

AuthUserSchema.method('createDefaultUser', function(username, displayName, callback){
  this.name = username;
  this.displayName = displayName;
  this.role = 'User';
  this.activeTokenId = '';
  this.isActive = true;
  this.permissions = PermissionCypher.getB64FromJsonString("[]");
  this.permissionHash = PermissionCypher.getHashFromJsonString("[]");
  //this.accessRights = [];

  this.save((err, doc) => {
    callback(this);
  });
});


module.exports = connection.model('AuthUser', AuthUserSchema, 'AuthUser');
