'use strict';
const promise = require('bluebird');
const SqlConnection = require("tedious").Connection;
const Request = require("tedious").Request;
const MssqlConnectionProvider = require("../core/mssql-connection-provider");

class SqlRepositoryBase {
    constructor(connectionName) {
        this.config = MssqlConnectionProvider.Current().getConnectionConfig(connectionName);
    }

    executeQuery(query) {
        let resultEntity = {result: {}, error: null};

        return new promise((resolve, reject) => {
            var connection = new SqlConnection(this.config);

            connection.on('connect', function (err) {
                let request = new Request(query, function (err, rowCount, rows) {
                    if (err) {
                        resultEntity.error = err;
                        reject(resultEntity);
                    } else {
                        resultEntity.result = rows;
                        resolve(resultEntity);
                    }

                    connection.close();
                });
                connection.execSql(request);
            }
           );
        });
    }
}

module.exports = SqlRepositoryBase;
