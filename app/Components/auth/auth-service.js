'use strict';

var passport = require('passport');
var passwordJwt = require("passport-jwt");
var AuthUser  = require('./auth-repository');
var uuid = require('node-uuid');
var ActiveDirectory = require('activedirectory');
var appConfig = require("../../config/settings/settings.app.json");
var TokenService = require('../../core/token-service');
var PermissionCypher = require('../../core/permissionCypher');
const promise = require('bluebird');

var adConfig = { url: appConfig.auth.ldapUrl, baseDN: appConfig.auth.baseDN, username: appConfig.auth.username, password: appConfig.auth.password};

function CreateTokenFromUser(user){
  var activeTokenId = uuid.v4();
  user.refreshUserActiveToken(activeTokenId);

  var payload = {id: user.id, email: user.email, role: user.role, modules: PermissionCypher.getJsonObjectFromB64(user.permissions) , name: user.name, displayName: user.displayName };
  var token = TokenService.refreshToken(activeTokenId, payload);

  return token;
}

var authService = {
  configure: function(application){
    var options = { secretOrKey : TokenService.getSecretKey(), jwtFromRequest : passwordJwt.ExtractJwt.fromAuthHeader()};
    passport.use(new passwordJwt.Strategy(options, authService.authenticate));
  },

  login: function (username, password, callback){
    var ad = new ActiveDirectory(adConfig);

    return new promise((resolve, reject) => {
      ad.authenticate(appConfig.auth.domainPrefix + username, password, (err, auth) => {
        if (err) {
          return reject();
        }

        if(auth){
          AuthUser.findOne({ name: username}).exec()
            .then (user => {
              if(!user) {
                ad.findUser(username, (error, userFinded) => {
                  if(error || !userFinded) {
                    return reject();
                  }

                  return new AuthUser().createDefaultUser(username, userFinded.displayName, (loginUser) => {
                    return resolve(CreateTokenFromUser(loginUser));
                  });
                })
              }
              else{
                if (!user.isActive) {
                  return reject();
                }

                if(PermissionCypher.verifyHash(user.permissionHash, user.permissions))
                  return resolve(CreateTokenFromUser(user));
                else
                  return reject();                  
              }
            })
          .catch(err => {
            console.log(err);
            reject();
          })
        }
        else {
          reject();
        }
      })
    });
  },

  refreshToken: function (req) {
    let jwt_payload = TokenService.extractToken(req);

    return new promise((resolve, reject) => {
      AuthUser.findOne({ name: jwt_payload.name}).exec()
        .then(user => {
          if(!user) {
            return reject();
          }

          var activeTokenId = uuid.v4();
          user.refreshUserActiveToken(activeTokenId);

          var token = TokenService.refreshToken(activeTokenId, jwt_payload);
          resolve(token);
        })
        .catch(err => {
          reject();
        })
    });
  },

  authenticate: function (jwt_payload, done) {
    AuthUser.findOne({ name: jwt_payload.name}, function(err, user){
      if(err) throw err;

      if (!user) {
        return done(null, false);
      }

      if(user.activeTokenId != jwt_payload.tokenId) {
        return done(null, false);
      }
      else{
        return done(null, user);
      }
    });
  },

  logout: function (req) {
    let jwt_payload = TokenService.extractToken(req);

    return new promise((resolve, reject) => {
      AuthUser.findOne({ name: jwt_payload.name}).exec()
        .then(user => {
          if(user) {
            user.rejectActiveToken(jwt_payload.name);
          }
          resolve();
        })
        .catch(err => {
          reject(err);
        })
      });
  }
};

module.exports = authService;
