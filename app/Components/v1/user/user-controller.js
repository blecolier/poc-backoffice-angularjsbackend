var AuthorizationService = require('../../../core/authorization-service');
var paginationParser = require('../../../core/paginationParser');
var PermissionCypher = require('../../../core/permissionCypher');
var AuthUser = require('../../auth/auth-repository');
var userService = require('./user-service');

var moduleName = 'UserManagement';
var role = 'Admin';

var userController = {

  get: (req, res, next) => {
    AuthorizationService.authorize(req, res, moduleName, role)
      .then(() => {
        AuthUser.paginate({}, paginationParser.parse(req.query), function(err, result){
          if(err) {
            res.status(200).json({success: false, msg: 'Error obtaining the users'});
            return;
          }

          result.docs = userService.getPublicInfo(result.docs);
          res.status(200).json({success: true, paginationResult: result });
        })
      });
  },

  put: (req, res, next) => {
    AuthorizationService.authorize(req, res, moduleName, role)
      .then(() => {
        permissionInfo = userService.transformPermissionInfo(req.body.modules);

        AuthUser.update({_id : req.params.userid},
            {$set: {
                  role : req.body.role,
                  isActive : req.body.isActive,
                  permissionHash : permissionInfo.Hash,
                  permissions : permissionInfo.Permissions
                }
            },
            function(err){
              if(err) {
                res.status(200).json({success: false, msg: 'Error updating the user'});
                return;
              }
            });

        res.status(200).json({success: true});
      });
    },

  delete: (req, res, next) => {
    AuthorizationService.authorize(req, res, moduleName, role)
      .then(() => {
        AuthUser.remove({_id : req.params.userid}, function(err){
          if(err) {
            res.status(200).json({success: false, msg: 'Error removing the user'});
            return;
          }
        });

        res.status(200).json({success: true});
      });
  }
};

module.exports = userController;
